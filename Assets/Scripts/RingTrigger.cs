﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This script will add to the player health when we fly through a ring
public class RingTrigger : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        //if we have collided with the player body we can subtract health
        if (other.gameObject.name.Equals("BodyMesh"))
        {
            GameManager.subtractPlayerHealth(-0.1f);
            changeColor();
        }
    }



    public void changeColor()
    {
        Material firstMaterial = transform.parent.GetChild(0).GetComponent<MeshRenderer>().material;
        firstMaterial.SetColor("_EmissionColor", Color.HSVToRGB(Random.Range(0f, 1f), 1, 1));
        for(int i = 0; i < transform.parent.childCount -1; i++)
        {
            transform.parent.GetChild(i).GetComponent<MeshRenderer>().material = firstMaterial;

        }


    }
}
