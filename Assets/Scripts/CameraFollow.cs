﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    //the current following state
    public static followingStates followState = followingStates.followingPlayer;
    public enum followingStates
    {
        followingPlayer,
        watchingPlayerDie
    }

    
    public GameObject player;

    // Update is called once per frame
    void FixedUpdate()
    {
        if(followState == followingStates.followingPlayer)
        {
            PlayerControl playerControlScript = player.GetComponent<PlayerControl>();

            ArrayList allLocationData = playerControlScript.savedLocations;

            if (allLocationData.Count > 0)
            {
                int lookBackFrames = 0;
                Vector3 targetPosition = (allLocationData[lookBackFrames] as LocationData).transform.position;
                Quaternion targetRotation = (allLocationData[lookBackFrames] as LocationData).transform.rotation;



                //method 1, be in the position from a couple frames ago
                transform.position = targetPosition;
                transform.position += transform.right * 20;
                transform.rotation = targetRotation;

                //method 2, stay a fixed distance behind the player while looking at them
                //transform.GetChild(0).transform.LookAt(player.transform.GetChild(0).transform.position, transform.up);
                //transform.GetChild(0).transform.position = player.transform.GetChild(0).transform.position + player.transform.GetChild(0).transform.right * 15;

            }
        }
        if(followState == followingStates.watchingPlayerDie)
        {

            PlayerControl playerControlScript = player.GetComponent<PlayerControl>();

            ArrayList allLocationData = playerControlScript.savedLocations;

            if (allLocationData.Count > 0)
            {
                //transform.position = (allLocationData[0] as LocationData).position;
                //transform.position += transform.right * 10;
                transform.position += transform.right * -2;
                transform.GetChild(0).transform.LookAt(player.transform.GetChild(0).transform.position,transform.up);
                //transform.rotation = (allLocationData[0] as LocationData).rotation;
            }



        }
        
    }

    //watches the player die while maintaining the same camera location
    public static void watchPlayerDie()
    {
        followState = followingStates.watchingPlayerDie;
    }
}
