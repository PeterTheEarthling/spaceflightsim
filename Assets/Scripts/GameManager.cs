﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class GameManager : MonoBehaviour
{
    public Transform DeathScene;

    //the current state
    public static gameStates gameState = gameStates.playingLevel1;
    public enum gameStates
    {
        playingLevel1,
        dead
    }
    //the health
    public Scrollbar healthScrollBar;

    public Material buildingWindows;

    //health of the player
    public static float playerHealth = 1;

    public void Initialize()
    {
        playerHealth = 1;
        gameState = gameStates.playingLevel1;
        startTime = Time.time;
        PlayerControl.playerState = PlayerControl.playerStates.alive;
        CameraFollow.followState = CameraFollow.followingStates.followingPlayer;
    }

    //Initializes scene
    private void Start()
    {
        Initialize();
    }
    // Update is called once per frame
    void Update() {
        //set the scroll bar percent to the player health
        updateHealthScrollBar();
        //see if the player is dead or not
        seeIfPlayerDead();

        rainBowBuildings();

    }

    float startTime = 0;
    private void rainBowBuildings()
    {
        float percent = (Time.time - startTime) / 60;
        if(percent > 1)
        {
            startTime = Time.time;
        }
        buildingWindows.SetColor("_EmissionColor", Color.HSVToRGB(percent, 1, 1)); 
    }

    //whatever player health is we set to the scrollbar
    private void updateHealthScrollBar()
    {
        healthScrollBar.size = playerHealth;
    }



    //subtracts health from the player
    public static void subtractPlayerHealth(float amount)
    {
        playerHealth -= amount;
        //don't allow more than 100%
        if(playerHealth > 1)
        {
            playerHealth = 1;
        }
        
    }


    //sees if the player has less than 0 health and manages that
    private void seeIfPlayerDead()
    {
        if (PlayerControl.playerState == PlayerControl.playerStates.dead &&
            gameState != gameStates.dead)
        {
            //SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene()); 
            //SceneManager.LoadScene("DeathScene", LoadSceneMode.Additive);
            DeathScene.gameObject.SetActive(true);
            gameState = gameStates.dead;//die

        }
    }


    public void restartLevel()
    {
        // SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        SceneManager.LoadScene("Level1Scene", LoadSceneMode.Single);
        //Application.LoadLevel(Application.loadedLevel);

        //Reset the Variables and then start the game over again
        Initialize();
    }





}
