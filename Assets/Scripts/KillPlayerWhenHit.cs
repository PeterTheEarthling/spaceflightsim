﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlayerWhenHit : MonoBehaviour
{
    public PlayerControl mainPlayerControl;
    
    //dead if we hit something
    void OnCollisionEnter(Collision col)
    {
        Object.Destroy(col.gameObject);
        return;
        //subtract a little if you are hit by a bullet
        if (col.gameObject.tag.ToString().Contains("Bullet"))
        {
           
                Debug.Log("subtracted health");
                GameManager.subtractPlayerHealth(0.3f);
            
        }
        else
        {
            Debug.Log("killed health");

            GameManager.playerHealth = 0;
            
        }
    }
}
