﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlayerControl : MonoBehaviour{
    Vector3 offsetCam;
    Quaternion offsetCamRot;

    public Transform lookAtMe;
    public Transform cameraLocation;
    public Transform playerBody;
    public Transform camera;
    public Transform bullet;

    //all the saved locations
    public ArrayList savedLocations = new ArrayList();

    public float movementSpeed = 500.0f;
    public float rollTorque = 500.0f;
    public float pullUpTorque = 500.0f;




    //player state data
    public static playerStates playerState = playerStates.alive;
    public enum playerStates
    {
        alive,
        dead
    }

    // Use this for initialization
    void Start () {

        //offsetCam = camera.position - playerBody.position;
    }

    /*
    //To hold score values
    private int score = 0;
    private int timeLeft;
    //this starts a countdown timer (timer1 should be 
    //an object placed in the viewing frame like 
    //health) that should end the game when you hit 
    //0. The rings give you an extra three seconds

    private void timer1_Tick(object sender, EventArgs e)
    {
        if (timeLeft > 0)
        {
            timeLeft = timeLeft - 1;
            timeLabel.Text = timeLeft + " seconds";
            score += 1;
        }
        
        if (//please put a collision with ring in here)
        {  
            timeLeft = timeLeft + 3;
            score += 2;
        }
    else
    {
            timer1.Stop();
            timeLabel.Text = "Out of Fuel";
            //add code for ending the game and ragdoll
            MessageBox.Show("Game Over. You Scored " + score + " Points.");
        }
    }


    public void StartTimer()
    {
        timeLeft = 30;
        timeLabel.Text = "30 seconds";
        timer1.Start();
    }
    */

    // Update is called once per frame
    new void FixedUpdate () {

        //savedLocations.Add(new LocationData(transform.GetChild(0).position, transform.GetChild(0).rotation));
        savedLocations.Add(new LocationData(transform.GetChild(0)));
        if (savedLocations.Count > 1)
        {
            savedLocations.RemoveAt(0);
        }

        if(GameManager.playerHealth <= 0)
        {
            killPlayer();
        }

        //camera.rotation = rotation;
        if (playerState == playerStates.alive)
        {
            doBullets();
            playerControl();
        }
        if(playerState == playerStates.dead)
        {
            
            
        }
        
    }


    
    //watches the player die
    public void killPlayer()
    {
        playerState = playerStates.dead;
        CameraFollow.watchPlayerDie();
        playerBody.GetComponent<Rigidbody>().drag = 0;
        playerBody.GetComponent<Rigidbody>().angularDrag = 0;
    }


    float lastFireTime = 0;
    public float secondsPerFire = 0.2f;
    private void doBullets()
    {
        float currTime = Time.time;
        if (Input.GetAxis("Fire1") > 0.5 && currTime - lastFireTime > secondsPerFire)
        {
            lastFireTime = currTime;
            Transform clone = Instantiate(bullet, playerBody.position, playerBody.rotation);
            clone.Rotate(new Vector3(0, 0, 90));
            clone.transform.position += clone.transform.up * 10;
            clone.GetComponent<Rigidbody>().AddRelativeForce(new Vector3(0, 14000, 0));
        }
    }
    private void playerControl()
    {
        playerBody.GetComponent<Rigidbody>()
            .AddRelativeTorque(new Vector3(Input.GetAxis("Horizontal") * rollTorque, 0, 0));

        playerBody.GetComponent<Rigidbody>()
            .AddRelativeTorque(new Vector3(0, 0, Input.GetAxis("Vertical") * pullUpTorque));

        float amountOfBoost = Input.GetAxis("Throttle");
        amountOfBoost = 1;
        playerBody.GetComponent<Rigidbody>()
            .AddRelativeForce(new Vector3((amountOfBoost-1)/2f * movementSpeed * 1, 0f, 0f));
        Debug.Log(Input.GetAxis("Throttle"));


        playerBody.GetComponent<Rigidbody>().AddRelativeForce(new Vector3(-movementSpeed, 0f, 0f));

       //ReadAxes();
    }
    /*
    private static void ReadAxes()
    {
        var inputManager = AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0];

        SerializedObject obj = new SerializedObject(inputManager);

        SerializedProperty axisArray = obj.FindProperty("m_Axes");

        if (axisArray.arraySize == 0)
            Debug.Log("No Axes");

        for(int i = 0; i< axisArray.arraySize; i++)
            {
            var axis = axisArray.GetArrayElementAtIndex(i);

            var name = axis.FindPropertyRelative("m_Name").stringValue;
            var axisVal = axis.FindPropertyRelative("axis").intValue;
            var inputType = (InputType) axis.FindPropertyRelative("type").intValue;

            Debug.Log(name);
            Debug.Log(axisVal);
            Debug.Log(inputType);
        }
    }

    public enum InputType
    {
        KeyOrMouseButton,
        MouseMovement,
        JoystickAxis,
    };
    */
}

/*
//saves the location of an object
public class LocationData
{
    public Vector3 position;
    public Quaternion rotation;
    public LocationData(Vector3 position, Quaternion rotation)
    {
        this.position = position;
        this.rotation = rotation;
    }
}
*/

//saves the location of an object
public class LocationData
{
    public Transform transform;
    public LocationData(Transform location)
    {
        GameObject emptyGO = new GameObject();

        transform = emptyGO.transform;
        transform.position = location.position;
        transform.rotation = location.rotation;

        //transform = location;
    }
}